import pymysql
from app import app
from tables import Customers
from db_config import mysql
from flask import flash, render_template, request, redirect

@app.route('/new_cust')
def add_cust_view():
	return render_template('add.html')
		
@app.route('/add', methods=['POST'])
def add_cust():
	conn = None
	cursor = None
	try:		
		_firstName = request.form['inputFirstName']
		_lastName = request.form['inputLastName']
		_emailAddr = request.form['inputEmailAddress']
		_phoneAddr = request.form['inputPhoneAddress']
		_addr = request.form['inputAddress']

		# validate the received values
		if _firstName and _lastName and _emailAddr and _phoneAddr and _addr and request.method == 'POST':

			# check if the first name and last name of the customer are existed in the database, 0: does not exist, 1: exist
			if check_dup(_firstName, _lastName) == 0:
				sql = "INSERT INTO customers(firstname, lastname, emailaddress, phoneaddress, address) VALUES(%s, %s, %s, %s, %s)"
				data = (_firstName, _lastName, _emailAddr, _phoneAddr, _addr,)
				conn = mysql.connect()
				cursor = conn.cursor()
				cursor.execute(sql, data)
				conn.commit()
				flash('Customer info created successfully!')				
			else:
				flash('Customer info is already existed!')

			return redirect('/')
		else:
			return 'Encountered error while creating customer'
	except Exception as e:
		print(e)
	finally:
		if cursor != None:
			cursor.close() 
		if conn != None:
			conn.close()
		
@app.route('/')
def cust():
	conn = None
	cursor = None
	try:
		conn = mysql.connect()
		cursor = conn.cursor(pymysql.cursors.DictCursor)
		cursor.execute("SELECT * FROM customers")
		rows = cursor.fetchall()
		table = Customers(rows)
		table.border = True
		return render_template('customers.html', table=table)
	except Exception as e:
		print(e)
	finally:
		cursor.close() 
		conn.close()

@app.route('/edit/<int:id>')
def edit_view(id):
	conn = None
	cursor = None
	try:
		conn = mysql.connect()
		cursor = conn.cursor(pymysql.cursors.DictCursor)
		cursor.execute("SELECT * FROM customers WHERE id=%s", id)
		row = cursor.fetchone()
		if row:
			return render_template('edit.html', row=row)
		else:
			return 'Encountered error while loading #{id}'.format(id=id)
	except Exception as e:
		print(e)
	finally:
		cursor.close()
		conn.close()

@app.route('/update', methods=['POST'])
def update_cust():
	conn = None
	cursor = None
	try:		
		_firstName = request.form['inputFirstName']
		_lastName = request.form['inputLastName']
		_emailAddr = request.form['inputEmailAddress']
		_phoneAddr = request.form['inputPhoneAddress']
		_addr = request.form['inputAddress']
		_id = request.form['id']
		# validate the received values
		if _firstName and _lastName and _emailAddr and _phoneAddr and _addr and request.method == 'POST':
			# check if the first name and last name of the customer are existed in the database, 0: does not exist, 1: exist
			if check_dup(_firstName, _lastName) == 0:
				sql = "UPDATE customers SET firstname=%s, lastname=%s, emailaddress=%s, phoneaddress=%s, address=%s  WHERE id=%s"
				data = (_firstName, _lastName, _emailAddr, _phoneAddr, _addr, _id,)
				conn = mysql.connect()
				cursor = conn.cursor()
				cursor.execute(sql, data)
				conn.commit()
				flash('Customer info updated successfully!')
			else:
				flash('Customer info is already existed!')
				return redirect('/')
		else:
			return 'Encountered error while updating customer'
	except Exception as e:
		print(e)
	finally:
		if cursor != None:
			cursor.close() 
		if conn != None:
			conn.close()
		
@app.route('/delete/<int:id>')
def delete_cust(id):
	conn = None
	cursor = None
	try:
		conn = mysql.connect()
		cursor = conn.cursor()
		cursor.execute("DELETE FROM customers WHERE id=%s", (id,))
		conn.commit()
		flash('Customer deleted successfully!')
		return redirect('/')
	except Exception as e:
		print(e)
	finally:
		cursor.close() 
		conn.close()

		
@app.route('/filter', methods=['POST'])
def filter_cust():
	conn = None
	cursor = None
	try:		
		_custId = request.form['inputCustId']

		# validate the received values
		if request.method == 'POST':
			if _custId:
				conn = mysql.connect()
				cursor = conn.cursor(pymysql.cursors.DictCursor)
				cursor.execute("SELECT * FROM customers WHERE id=%s", _custId)
				rows = cursor.fetchall()
				table = Customers(rows)
				table.border = True
				return render_template('customers.html', table=table)
			else:
				return redirect('/')
		else:
			return 'Encountered error while creating customer'
	except Exception as e:
		print(e)
	finally:
		if cursor != None:
			cursor.close() 
		if conn != None:
			conn.close()

def check_dup(firstname, lastname):
	conn = None
	cursor = None
	try:
		sql = "SELECT * FROM customers WHERE firstname=%s and lastname=%s"
		data = (firstname, lastname,)
		conn = mysql.connect()
		cursor = conn.cursor()
		cursor.execute(sql, data)
		row = cursor.fetchone()
		if row:
			return 1
		else:
			return 0
	except Exception as e:
		print(e)
	finally:
		cursor.close()
		conn.close()
		
if __name__ == "__main__":
    app.run()