CREATE DATABASE IF NOT EXISTS `customer_info_management`;

USE customer_info_management;

CREATE TABLE `customers`(
`id` int NOT NULL AUTO_INCREMENT,
`firstname` varchar(100) NOT NULL,
`lastname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
`emailaddress` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
`phoneaddress` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
`address` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

