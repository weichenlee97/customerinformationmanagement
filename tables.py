from flask_table import Table, Col, LinkCol
 
class Customers(Table):
    id = Col('Customer Id')
    firstname = Col('First Name')
    lastname = Col('Last Name')
    emailaddress = Col('Email Address')
    phoneaddress = Col('Phone No')
    address = Col('Address')
    edit = LinkCol('Edit', 'edit_view', url_kwargs=dict(id='id'))
    delete = LinkCol('Delete', 'delete_cust', url_kwargs=dict(id='id'))
